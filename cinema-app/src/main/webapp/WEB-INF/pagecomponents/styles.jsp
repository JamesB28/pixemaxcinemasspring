<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<link rel="stylesheet" type="text/css" href="<c:url value='/resources/bootstrap/css/bootstrap.min.css'/>">
<link rel="stylesheet" type="text/css" href="<c:url value='http://code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css'/>">
<link rel="stylesheet" type="text/css" href="<c:url value='/resources/css/styles.css'/>">
<!-- JS includes can be moved to footer for faster page loading -->
<script src="http://code.jquery.com/jquery-1.11.0.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<c:url value='http://code.jquery.com/ui/1.9.2/jquery-ui.min.js'/>"></script>
<script type="text/javascript" src="<c:url value='/resources/bootstrap/js/bootstrap.min.js'/>"></script>
<script type="text/javascript" src="<c:url value='/resources/snap.svg-min.js'/>"></script>
