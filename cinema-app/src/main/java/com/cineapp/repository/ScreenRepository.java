package com.cineapp.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.cineapp.model.Screen;

public interface ScreenRepository extends CrudRepository<Screen, Long>
{
	public List<Screen> findByCinemaId(long id);
}
