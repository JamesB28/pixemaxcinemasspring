package com.cineapp.service.test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.testng.annotations.Test;

import static org.testng.Assert.*;
import static org.mockito.Mockito.*;

import com.cineapp.model.Cinema;
import com.cineapp.model.Film;
import com.cineapp.repository.CinemaRepository;
import com.cineapp.repository.FilmRepository;
import com.cineapp.service.FilmService;
import com.cineapp.service.FilmServiceImpl;
import com.cineapp.testcomponents.TestBase;

public class FilmServiceTest extends TestBase
{
	@Mock
	private FilmRepository filmRepositoryMock;
	
	@Mock
	private CinemaRepository cinemaRepositoryMock;
	
	@InjectMocks
	private FilmService filmServiceMock = new FilmServiceImpl();
	
	/**
	 * Ensure that an invalid cinema id retrieves no data from the 
	 * data layer, and returns null.
	 */
	@Test
	public void getCurrentFilmsForCinemaTest_InvalidCinemaId()
	{
		//Arrange
		long cinemaId = 0;
		
		//Act
		Set<Film> films = filmServiceMock.getCurrentFilmsForCinema(cinemaId);
		
		//Assert
		verify(cinemaRepositoryMock, never()).findByIdFetchFilms(anyLong());
		assertNull(films);
	}
	
	/**
	 * Ensure that films are retrieved for a valid cinema id
	 */
	@Test
	public void getCurrentFilmsForCinemaTest_ValidCinemaId()
	{
		//Arrange
		long cinemaId = 4l;
		Cinema cinema = getCinemaWithFilms();
		when(cinemaRepositoryMock.findByIdFetchFilms(cinemaId)).thenReturn(cinema);
		
		//Act
		Set<Film> films = filmServiceMock.getCurrentFilmsForCinema(cinemaId);
		
		//Assert
		verify(cinemaRepositoryMock, times(1)).findByIdFetchFilms(cinemaId);
		assertEquals(2, films.size());
		assertEquals(cinema.getFilms().toArray()[0], films.toArray()[0]);
		assertEquals(cinema.getFilms().toArray()[1], films.toArray()[1]);
	}
	
	/**
	 * Ensure that the data access layer correctly has a request made
	 * to retrieve all films.
	 */
	@Test
	public void getAllFilmsTest_success()
	{
		//Arrange
		when(filmRepositoryMock.findAll()).thenReturn((ArrayList<Film>)getSampleFilms());
		
		//Act
		List<Film> films = filmServiceMock.getAllFilms();
				
		//Assert
		assertEquals(2, films.size());
	}
	
	/**
	 * Ensure that if null is passed in when attempting to create 
	 * a new {@link Film} object, the service returns false.
	 */
	@Test
	public void addNewFilmTest_FilmNull()
	{
		//Arrange
		
		//Act
		boolean result = filmServiceMock.addNewFilm(null);
		
		//Assert
		assertFalse(result);
	}
	
	/**
	 * Ensure that when an attempt to save a new Film is made, if the repository
	 * returns null the service returns false.
	 */
	@Test
	public void addNewFilmTest_RepositoryReturnsNull()
	{
		//Arrange
		Film film = new Film();
		when(filmRepositoryMock.save(film)).thenReturn(null);
		
		//Act
		boolean result = filmServiceMock.addNewFilm(film);		
		
		//Assert
		verify(filmRepositoryMock, times(1)).save(film);
		assertFalse(result);
	}
	
	/**
	 * Ensure the film to persist has the correct {@link Cinema} objects populated
	 * before it is sent to the service. 
	 */
	@Test
	public void addNewFilmTest_Success()
	{
		//Arrange
		Film film = new Film();
		Set<Long> cinemaIds = new TreeSet<>();
		cinemaIds.add(5l);
		cinemaIds.add(11l);
		film.setCinemaIds(cinemaIds);
		when(filmRepositoryMock.save(film)).thenReturn(film);
		ArgumentCaptor<Film> filmCaptor = ArgumentCaptor.forClass(Film.class);
		
		//Act
		boolean result = filmServiceMock.addNewFilm(film);		
		
		//Assert
		verify(filmRepositoryMock, times(1)).save(filmCaptor.capture());
		assertTrue(result);
		assertEquals(2, filmCaptor.getValue().getCinemas().size());
		assertEquals(11l, ((Cinema)filmCaptor.getValue().getCinemas().toArray()[0]).getId().longValue());
		assertEquals(5l, ((Cinema)filmCaptor.getValue().getCinemas().toArray()[1]).getId().longValue());
	}
	
	/**
	 * @return a {@link Cinema} object populated with two {@link Film} objects
	 */	
	private Cinema getCinemaWithFilms()
	{
		Cinema cinema = new Cinema();
		cinema.setFilms(new HashSet<Film>(getSampleFilms()));
		return cinema;
	}
	
	/**
	 * @return a <code>Set</code> of two {@link Film} objects.
	 */
	private List<Film> getSampleFilms()
	{
		Film film1 = new Film();
		film1.setName("film1");
		Film film2 = new Film();
		film2.setName("film2");
		List<Film> filmSet = new ArrayList<>();
		filmSet.add(film1);
		filmSet.add(film2);
		return filmSet;
	}
}
