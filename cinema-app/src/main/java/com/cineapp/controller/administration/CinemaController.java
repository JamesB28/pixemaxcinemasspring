package com.cineapp.controller.administration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.cineapp.model.Cinema;
import com.cineapp.model.Screen;
import com.cineapp.model.uicomposite.CinemaCreation;
import com.cineapp.model.validators.CinemaCreationValidator;
import com.cineapp.service.CinemaService;

/**
 * Controller to manage the creation and modification of Cinemas that have 
 * been registered within the system
 * @author James Butler
 */
@Controller
@RequestMapping("/admin/cinema")
public class CinemaController 
{
	@Autowired
	private CinemaService cinemaService;
	
	@Autowired
	private CinemaCreationValidator cinemaCreationValidator;
	
	private static final String LIST_URL = "/list";
	private static final String CREATE_URL = "/create";
	private static final String LIST_FULL_URL = "/admin/cinema/list";
	private static final String CREATE_FULL_URL = "/admin/cinema/create";
	
	/**
	 * Retrieves a list of all available Cinemas 
	 * @return A <code>ModelAndView</code> object containing the URL for the cinema list page, 
	 * along with a collection of all <code>Cinema</code> objects retrieved from the database.
	 */
	@RequestMapping(value=LIST_URL, method = RequestMethod.GET)
	public ModelAndView getCinemaList()
	{
		ModelAndView modelAndView = new ModelAndView(LIST_FULL_URL); 
		modelAndView.addObject("cinemas", cinemaService.findAll());
		return modelAndView;
	}
	
	/**
	 * Directs the user to the Cinema creation screen
	 * @return A <code>ModelAndView</code> object redirecting the user to the Cinema Create page
	 */
	@RequestMapping(value=CREATE_URL, method = RequestMethod.GET)
	public ModelAndView getCreateCinemaForm()
	{
		ModelAndView modelAndView = new ModelAndView(CREATE_FULL_URL);
		modelAndView.addObject("cinemaCreationObj", new CinemaCreation(new Cinema(), GetNumberOfScreensDropdown()));
		return modelAndView;
	}
	
	/**
	 * Pass the <code>Cinema</code> object posted from the form to the service layer
	 * @param cinema
	 * @return
	 */
	@RequestMapping(value=CREATE_URL, method = RequestMethod.POST)
	public ModelAndView createNewCinema(
			@ModelAttribute("cinemaCreationObj") CinemaCreation cinemaCreationObj,
			BindingResult result)
	{
		cinemaCreationValidator.validate(cinemaCreationObj, result);
		
		if (result.hasErrors())
		{
			ModelAndView modelAndView = new ModelAndView(CREATE_FULL_URL);
			cinemaCreationObj.setScreenDropdownValues(GetNumberOfScreensDropdown());
			modelAndView.addObject("cinemaCreationObj", cinemaCreationObj);
			return modelAndView;
		}
		else
		{
			Cinema newCinema = cinemaCreationObj.getCinema();
			
			for(int i = 0; i < cinemaCreationObj.getNumScreens(); i++)
			{
				newCinema.getScreens().add(new Screen(i + 1, newCinema));
			}
			
			cinemaService.save(newCinema);
		}
		return getCinemaList();
	}
	
	/**
	 * Instantiate a list of integers ranging from 1 to the max value which by default is 20.
	 * @return A <code>int[]</code> with sequential values from 1 to 20 
	 */
	private int[] GetNumberOfScreensDropdown()
	{
		//The max no. screens value could be added to a properties configuration
		int[] noScreens = new int[20]; 
		for(int i = 0; i<noScreens.length; i++)
		{
			noScreens[i] = i + 1;
		}
		return noScreens;					
	}
}
