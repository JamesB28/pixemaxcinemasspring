<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="joda" uri="http://www.joda.org/joda/time/tags" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<h2><spring:message code="film_create.title"/></h2>
<c:if test="${not empty errorMessage}">
	<div class="alert alert-success alert-dismissable"> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><spring:message code="${errorMessage}"/></div>
</c:if>
<form:form method="POST" modelAttribute="film" class="form-horizontal" role="film">
	<input name="showingFrom" type="hidden" id="showingFrom" value="<joda:format value="${film.showingFrom}" style="S-" pattern="yy/M/d"/>">
	<input name="showingTo" type="hidden" id="showingTo" value="<joda:format value="${film.showingTo}" style="S-" pattern="yy/M/d"/>">
	
	<div class="form-group">
		<label for="name" class="col-sm-2 control-label"><spring:message code="general_label.name"/></label>
		<div class="col-sm-5">
			<form:input path="name" class="form-control"/>
		</div>
		<p class="help-block"><form:errors path="name" cssClass="error"/></p>
	</div>
	<div class="form-group">
		<label for="description" class="col-sm-2 control-label"><spring:message code="general_label.description"/></label>
		<div class="col-sm-5">
			<form:textarea path="description" class="form-control"/>
		</div>
		<p class="help-block"><form:errors path="description" cssClass="error"/></p>
	</div>
	<div class="form-group">
		<label for="runTime" class="col-sm-2 control-label"><spring:message code="film.run_time"/> <small><spring:message code="general_label.minutes"/></small></label>
		<div class="col-sm-2">
			<form:input path="runTime" class="form-control"/>
		</div>
		<p class="help-block"><form:errors path="runTime" cssClass="error"/></p>
	</div>
	<div class="form-group">
		<label for="showingFrom" class="col-sm-2 control-label"><spring:message code="film.showing_from"/></label>
		<div class="col-sm-3">
			<div class="input-group">
				<input name="showingFromDate" type="text" id="showingFromDate" class="form-control" disabled>
				<span class="input-group-btn">
					<button id="showingFromDateBtn" class="btn btn-default" type="button">&nbsp;<span class="glyphicon glyphicon-calendar"></span>&nbsp;</button>
				</span>
			</div>			
		</div>
		<p class="help-block"><form:errors path="showingFrom" cssClass="error"/></p>
	</div>
	<div class="form-group">
		<label for="showingTo" class="col-sm-2 control-label"><spring:message code="film.showing_to"/></label>
		<div class="col-sm-3">
			<div class="input-group">
				<input name="showingToDate" type="text" id="showingToDate" class="form-control" disabled>
				<span class="input-group-btn">
					<button id="showingToDateBtn" class="btn btn-default" type="button">&nbsp;<span class="glyphicon glyphicon-calendar"></span>&nbsp;</button>
				</span>
			</div>
		</div> 
		<p class="help-block"><form:errors path="showingTo" cssClass="error"/></p>
	</div>
	<div class="row">
		<div class="col-sm-offset-1 col-sm-3">
			<h3><spring:message code="film.showing_at"/>:</h3>
		</div>
	</div>
	<div class="form-group">
		<div class="col-sm-offset-2 col-sm-5">
			<c:set var="cinemaIdError"><form:errors path="cinemaIds" /></c:set>
			<c:if test="${not empty cinemaIdError}">
				<div class="alert alert-danger">${cinemaIdError}</div>
			</c:if>
			<div class="input-group">
				<c:if test="${empty cinemaList}">
				    <spring:message code="film.no_cinemas_available"/>
				    <input type="hidden" name="cinemaIds"/>
				</c:if>
			
				<c:forEach var="cinema" items="${cinemaList}">
					<div class="checkbox">	
						<form:checkbox path="cinemaIds" value="${cinema.id}" label="${cinema.name}"/>
					</div>
				</c:forEach>
			</div>				
		</div>
	</div>	
	<div class="form-group">
    	<div class="col-sm-8">
			<button type="submit" class="btn btn-success pull-right"><span class="glyphicon glyphicon-ok-circle"></span> <spring:message code="general_label.add"/></button>
		</div>
	</div>
</form:form>