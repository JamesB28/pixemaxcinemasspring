package com.cineapp.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "Cinema")
public class Cinema implements Serializable
{
	private static final long serialVersionUID = 1219699835111787783L;
	
	private Long id;
	private String name;
	private String city;
	private List<Screen> screens;
	private Set<Film> films;
	
	public Cinema(){}
	
	public Cinema(long id)
	{
		this.id = id;
	}
	
	public Cinema(Long id, String name)
	{
		this.id = id;
		this.name = name;
	}
	 
	@Id 
	@GeneratedValue
	@Column(name = "id")
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	@Column(name = "name", length = 50, nullable = false)
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@Column(name = "city", length = 100, nullable = false)
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "cinema")
	public List<Screen> getScreens() {
		if (screens == null)
		{
			screens = new ArrayList<Screen>();
			return screens;
		}
		return screens;
	}
	public void setScreens(List<Screen> screens) {
		this.screens = screens;
	}

	@ManyToMany
	@JoinTable(name = "cinemaFilms",
			joinColumns = @JoinColumn(name = "cinemaId", referencedColumnName = "id"),
			inverseJoinColumns = @JoinColumn(name = "filmId", referencedColumnName = "id"))
	public Set<Film> getFilms() {
		return films;
	}
	public void setFilms(Set<Film> films) {
		this.films = films;
	}
	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
