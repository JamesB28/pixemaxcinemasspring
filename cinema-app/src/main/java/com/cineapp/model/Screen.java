package com.cineapp.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;

@Entity
@Table(name = "Screen")
public class Screen implements Serializable
{
	private static final long serialVersionUID = 7784968267068875156L;

	private Long id;
	private String name;
	private int numRows;
	private int numSeatsPerRow;
	private Cinema cinema;
	private List<Showing> showing;
	//excluded seats property
	
	public Screen()
	{}
	
	/**
	 * Constructor to create a new default screen, where the screenNumber provided is included in the name
	 * and the object is associated to the cinema specified.
	 * @param screenNumber The number of the screen, this will by default create the name 'Screen <i>screenNumber</i>'.
	 * @param cinema The {@link Cinema} object to link the newly created <code>Screen</code> to.
	 */
	public Screen(int screenNumber, Cinema cinema)
	{
		this.name = "Screen " + screenNumber;
		this.numRows = 20;
		this.numSeatsPerRow = 20;
		this.cinema = cinema;
	}
	
	@Id 
	@GeneratedValue
	@Column(name = "id")
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	@Column(name = "name", length = 50, nullable = false)
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@Column(name = "numberOfRows", nullable = false)
	public int getNumRows() {
		return numRows;
	}
	public void setNumRows(int numRows) {
		this.numRows = numRows;
	}
	
	@Column(name = "numberOfSeatsPerRow", nullable = false)
	public int getNumSeatsPerRow() {
		return numSeatsPerRow;
	}
	public void setNumSeatsPerRow(int numSeatsPerRow) {
		this.numSeatsPerRow = numSeatsPerRow;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cinemaId")
	@ForeignKey(name = "FK_Cinema_Screens")
	public Cinema getCinema() {
		return cinema;
	}
	public void setCinema(Cinema cinema) {
		this.cinema = cinema;
	}
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "screen")
	public List<Showing> getShowing() {
		return showing;
	}
	public void setShowing(List<Showing> showing) {
		this.showing = showing;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
