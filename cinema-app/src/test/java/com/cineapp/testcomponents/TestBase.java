package com.cineapp.testcomponents;

import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;

/**
 * Class to implement features shared by all unit tests, e.g. mock injection
 */
public abstract class TestBase 
{

	@BeforeMethod(alwaysRun = true)
	public void injectDependencies()
	{
		MockitoAnnotations.initMocks(this);
	}
}
