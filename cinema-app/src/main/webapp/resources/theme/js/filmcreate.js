$(document).ready(function() { 
	$("#showingFromDate").datepicker({
		dateFormat: 'yy/mm/dd',
		constrainInput: true
	});
	$("#showingToDate").datepicker({
		dateFormat: 'yy/mm/dd',
		constrainInput: true
	});
	
	$("#showingFromDate").datepicker("setDate", $("#showingFrom").val());
	$("#showingToDate").datepicker("setDate", $("#showingTo").val());
	
	$("#showingFromDateBtn").on('click', function(){		
		$("#showingFromDate").datepicker("show");
	});
	
	$("#showingToDateBtn").on('click', function(){		
		$("#showingToDate").datepicker("show");
	});
});

$("#film").submit(function(e){
	e.preventDefault();
	var form = this;
	console.log('Showing from value: ' + $("#showingFromDate").val());
	console.log('Showing to value: ' + $("#showingToDate").val());		
	$("#showingFrom").val($("#showingFromDate").val());
	$("#showingTo").val($("#showingToDate").val());
	console.log($("#showingFrom").val());
	console.log($("#showingTo").val());
	form.submit();
});