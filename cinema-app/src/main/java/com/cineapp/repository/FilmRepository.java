package com.cineapp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.cineapp.model.Film;

public interface FilmRepository extends CrudRepository<Film, Long>
{
	/**
	 * Retrieve all unique {@link Film} entries for the {@link Cinema} as specified by id. 
	 * @param id The id of the {@link Cinema} to find films for
	 * @return A <code>List</code> of {@link Film} objects showing at the specified {@link Cinema}
	 */
	@Query("SELECT DISTINCT f FROM Film f "
			+ "JOIN FETCH f.showing sh "
			+ "JOIN sh.screen sc "
			+ "JOIN sc.cinema c "
			+ "WHERE c.id = ?1 ")
	public List<Film> findByCinemaId(long id);
}
