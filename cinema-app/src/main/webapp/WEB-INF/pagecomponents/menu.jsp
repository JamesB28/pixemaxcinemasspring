<nav class="navbar navbar-default" role="navigation">
	<div class="container-fluid">
	  	<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
	    	<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
	    		<span class="sr-only">Toggle navigation</span>
	        	<span class="icon-bar"></span>
	        	<span class="icon-bar"></span>
	       		<span class="icon-bar"></span>
	      	</button>
	      	<a class="navbar-brand" href="${pageContext.servletContext.contextPath}">Home</a>
		</div>
	     <!-- Collect the nav links, forms, and other content for toggling -->
	    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	    	<ul class="nav navbar-nav">
	      		<li><a href="#">What's on?</a></li>
	        	<li><a href="#">Book a Showing</a></li>
	      	</ul>
	      	<ul class="nav navbar-nav navbar-right">
	        	<li class="dropdown">
	          		<a href="#" class="dropdown-toggle" data-toggle="dropdown">Administration <b class="caret"></b></a>
	          		<ul class="dropdown-menu">
	            		<li><a href="${pageContext.servletContext.contextPath}/admin/cinema/list">Cinemas</a></li>
	            		<li><a href="${pageContext.servletContext.contextPath}/admin/film/list">Films</a></li>
	           		 	<li class="divider"></li>
	            		<li><a href="#">Refund Tickets</a></li>
	          		</ul>
	        	</li>
	      	</ul>
	   	</div><!-- /.navbar-collapse -->
	</div><!-- /.container-fluid -->
</nav>