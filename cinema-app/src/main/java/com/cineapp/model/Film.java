package com.cineapp.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.NotEmpty;
import org.joda.time.DateTime;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "Film")
public class Film implements Serializable
{
	private static final long serialVersionUID = -3437479418430852314L;
	
	private long id;
	private String name;
	private String description;
	private Integer runTime;
	private DateTime showingFrom;
	private DateTime showingTo;
	private Set<Showing> showing;
	private Set<Cinema> cinemas;
	
	/* Non database model properties */
	private Set<Long> cinemaIds;
	
	public Film(){}

	@Id 
	@GeneratedValue
	@Column(name = "id")
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	
	@NotEmpty(message = "The film must have a title")
	@Column(name = "name", length = 200, nullable = false)
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	@NotEmpty(message = "The film must have a description")
	@Column(name = "description", length = 500, nullable = false)
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

	@Min(1)
	@NotNull(message = "The film must have a run time")
	@Column(name = "runTime", nullable = false)
	public Integer getRunTime() {
		return runTime;
	}
	public void setRunTime(Integer runTime) {
		this.runTime = runTime;
	}

	@NotNull(message = "The showing from date cannot be empty")
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	@Column(name = "showingFrom", nullable = false)
	@DateTimeFormat(pattern = "yyyy/MM/dd")
	public DateTime getShowingFrom() {
		return showingFrom;
	}
	public void setShowingFrom(DateTime showingFrom) {
		this.showingFrom = showingFrom;
	}

	@NotNull(message = "The showing to date cannot be empty")
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	@Column(name = "showingTo", nullable = false)
	@DateTimeFormat(pattern = "yyyy/MM/dd")
	public DateTime getShowingTo() {
		return showingTo;
	}
	public void setShowingTo(DateTime showingTo) {
		this.showingTo = showingTo;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "film")
	public Set<Showing> getShowing() {
		return showing;
	}
	public void setShowing(Set<Showing> showing) {
		this.showing = showing;
	}

	@ManyToMany
	@JoinTable(name = "cinemaFilms",
			joinColumns = @JoinColumn(name = "filmId", referencedColumnName = "id"),
			inverseJoinColumns = @JoinColumn(name = "cinemaId", referencedColumnName = "id"))
	public Set<Cinema> getCinemas() {
		return cinemas;
	}
	public void setCinemas(Set<Cinema> cinemas) {
		this.cinemas = cinemas;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/* Non database model properties */	
	@Transient
	public Set<Long> getCinemaIds() {
		return cinemaIds;
	}

	public void setCinemaIds(Set<Long> cinemaIds) {
		this.cinemaIds = cinemaIds;
	}	
}
