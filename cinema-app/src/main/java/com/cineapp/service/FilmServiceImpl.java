package com.cineapp.service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cineapp.model.Cinema;
import com.cineapp.model.Film;
import com.cineapp.repository.CinemaRepository;
import com.cineapp.repository.FilmRepository;

@Service
public class FilmServiceImpl implements FilmService
{
	@Autowired
	private FilmRepository filmRepository;
	
	@Autowired
	private CinemaRepository cinemaRepository;
	
	/*
	 * (non-Javadoc)
	 * @see com.cineapp.service.FilmService#GetCurrentFilmsForCinema(long)
	 */
	public Set<Film> getCurrentFilmsForCinema(long cinemaId) 
	{
		if (cinemaId > 0)
		{	
			Cinema cinema = cinemaRepository.findByIdFetchFilms(cinemaId);
			
			//Null the showings until needed, causes lazy load issue 
			for(Film film : cinema.getFilms())
			{
				film.setShowing(null);
				film.setCinemas(null);
				film.setShowingFrom(null);
				film.setShowingTo(null);
			}
			return cinema.getFilms();
		}
		else
		{
			return null;
		}
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.cineapp.service.FilmService#GetAllFilms()
	 */
	public List<Film> getAllFilms()
	{
		return (List<Film>) filmRepository.findAll();
	}

	/*
	 * (non-Javadoc)
	 * @see com.cineapp.service.FilmService#AddNewFilm(com.cineapp.model.Film, long[])
	 */
	public boolean addNewFilm(Film film) 
	{
		if (film == null)
		{
			return false;
		}
		
		if (film.getCinemas() == null)
		{
			film.setCinemas(new HashSet<Cinema>());
		}
		
		if (film.getCinemaIds() != null)
		{
			for(long cinemaId : film.getCinemaIds())
			{
				film.getCinemas().add(new Cinema(cinemaId));
			}
		}
		
		if (filmRepository.save(film) != null)
		{
			return true;
		}
		return false;
	}
}
