package com.cineapp.service;

import java.util.List;

import com.cineapp.model.Cinema;

/**
 * This service is responsible for the retrieval and persistence of Cinemas within the system.
 */
public interface CinemaService {

	/**
	 * Retrieve all <code>Cinema</code> objects existing in the system
	 * @return A List of {@link Cinema}objects.
	 */
	public List<Cinema> findAll();
	
	/**
	 * Save a new Cinema object in the database.
	 * @param cinema The {@link Cinema} object to save.
	 */
	public void save(Cinema cinema);
}
