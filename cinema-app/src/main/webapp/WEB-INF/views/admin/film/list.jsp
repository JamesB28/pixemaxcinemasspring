<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="joda" uri="http://www.joda.org/joda/time/tags" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<h2>Film List</h2>
<c:if test="${not empty successMessage}">
	<div class="alert alert-success alert-dismissable"> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>${filmName} <spring:message code="${successMessage}"/></div>
</c:if>
<p>
	<a class="btn btn-success" href="${pageContext.servletContext.contextPath}/admin/film/create"><span class="glyphicon glyphicon-plus-sign"></span> <spring:message code="general_label.add"/></a>
</p>
<table class="table table-hover">
	<tr>
		<th>Name</th>
		<th>Description</th>
		<th>Run Time <small>minutes</small></th>
		<th>Showing From</th>
		<th>Showing To</th>
		<th></th>
	</tr>
	<c:forEach var="film" items="${films}">
		<tr>
			<td><c:out value="${film.name}" /></td>
			<td><c:out value="${film.description}" /></td>
			<td><c:out value="${film.runTime}" /></td>
			<td><joda:format value="${film.showingFrom}" style="S-" pattern="yyyy/M/d"/></td>
			<td><joda:format value="${film.showingTo}" style="S-" pattern="yyyy/M/d"/></td>			
			<td><a class="btn btn-info" href="#"><span class="glyphicon glyphicon-cog"></span> Edit Film</a></td>
		</tr>
	</c:forEach>
</table>