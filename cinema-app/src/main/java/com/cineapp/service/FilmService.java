package com.cineapp.service;

import java.util.List;
import java.util.Set;

import com.cineapp.model.Film;

public interface FilmService 
{
	/**
	 * Retrieve a list of films available for the {@link Cinema} as specified
	 * by cinemaId
	 * @param cinemaId The id of the {@link Cinema} to find films for
	 * @return A <code>Set</code> of {@link Film}s
	 */
	Set<Film> getCurrentFilmsForCinema(long cinemaId);
	
	/**
	 * Retrieve all films available in the system.
	 * @return A <code>List</code> of {@link Film}s
	 */
	List<Film> getAllFilms();
	
	/**
	 * Add a new {@link Film} object to the system, creating association links 
	 * to each of the {@link Cinema} objects as specified in the <code>cinemaIds</code>
	 * property of the model
	 * @param film The new {@link Film} object to create
	 * @return <code>true</code> if operation successful, <code>false</code> otherwise.
	 */
	boolean addNewFilm(Film film);
}
