package com.cineapp.model.uicomposite;

import java.util.Set;

import com.cineapp.model.Film;

/**
 * Class to display a collection of Films retrieved for a specific cinema
 * @author James Butler
 */
public class CinemaFilmListing 
{
	private final Set<Film> films;
	private final long cinemaId;
	
	public CinemaFilmListing(Set<Film> films, long cinemaId)
	{
		super();
		this.films = films;
		this.cinemaId = cinemaId;
	}

	public Set<Film> getFilms() 
	{
		return films;
	}

	public long getCinemaId() {
		return cinemaId;
	}
}
