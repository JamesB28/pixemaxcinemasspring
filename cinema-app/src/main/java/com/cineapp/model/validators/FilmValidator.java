package com.cineapp.model.validators;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.cineapp.model.Film;

@Component
public class FilmValidator implements Validator 
{

	@Override
	public boolean supports(Class<?> clazz) 
	{		
		return Film.class.equals(clazz);
	}

	@Override
	public void validate(Object obj, Errors errors) 
	{	
		Film film = (Film) obj;
		
		if (film.getShowingFrom().isAfter(film.getShowingTo()))
		{
			errors.rejectValue("showingFrom", "film.showing_from_before_showing_to", "The showing from date must be before the showing to date");
		}
		
		if (film.getCinemaIds() == null || film.getCinemaIds().size() <= 0)
		{
			errors.rejectValue("cinemaIds", "film.must_be_showing_in_at_least_one_cinema", "The film must be showing in at least one cinema");
		}
	}
}
