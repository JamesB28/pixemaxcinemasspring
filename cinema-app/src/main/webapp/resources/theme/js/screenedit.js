var rgbBlue = "rgb(0, 100, 255)";
var rgbBrown = "rgb(100, 50, 0)";

$(document).ready(function() { 
	
	//add onclick to every screen screenDesignId button to run the generateScreenDesigner function
	
	$('*[id*=designScreenBtn]').each(function() {		
		var elementId = "#" + this.id;
	    $(elementId).on('click', function(){
	    	var screenId = elementId.replace( /^\D+/g, '');
	    	var numSeats = $("#numSeatsScreen"+screenId).val();
	    	var numRows = $("#numRowsScreen"+screenId).val();    	
	    	generateScreenDesigner(screenId, numRows, numSeats);
	    });
	});
	
//	var s = Snap("#snapsvg");
//
//	var seats = [];
//	
//	for (var i=0; i<5; i++)
//	{
//		for (var j=0; j<10; j++)
//		{
//		    var rect = s.rect(j*11, i*11, 10, 10).attr({
//		        fill: rgbBlue
//		    });
//		    rect.click(changeSeatType);
//		    seats.push(rect);
//		}
//	}
	
//	var numRows = 10;
//	var numSeats = 20;
//	
//	for (var i=numRows; i>0; i--)
//	{
//		for (var j=numSeats; j>0; j--)
//		{
//			var rect = s.rect(j*21, i*21, 20, 20).attr({
//		        fill: rgbBlue
//		    });
//			
//			rect.data(""+i+j, "seat");
//		    rect.click(changeSeatType);
//		    seats.push(rect);
//		}
//	}
	
//	for(var i=0; i<seats.length; i++)
//	{
//		//console.log(seats[i].data());
//	}
});

/*
 * 
 */
function generateScreenDesigner(screenId, numRows, numSeats){
	
	var s = Snap("#screenDesignId" + screenId);
	//console.log($("#screenDesignId" +screenId).height);
	//$("#screenDesignId" +screenId).setAttribute('width', numRows * 20);

	var seats = [];
	
	for (var i=numRows; i>0; i--)
	{
		for (var j=numSeats; j>0; j--)
		{
			var rect = s.rect(j*21, i*21, 20, 20).attr({
		        fill: rgbBlue
		    });
			
			rect.data(""+i+j, i+j+"s");
		    rect.click(changeSeatType);
		    seats.push(rect);
		}
	}
	
	for(var i=0; i<seats.length; i++)
	{
		//console.log(seats[i].data());
	}
}

function changeSeatType(e){
	if (this.attr("fill") == rgbBlue){
		this.attr({
	        fill: rgbBrown
	    }); 
	} else {
		this.attr({
	        fill: rgbBlue
	    }); 
	}
}
