package com.cineapp.service;

import java.util.List;

import com.cineapp.model.Screen;

/**
 * SPI for the management of {@link Screen} objects
 * @author James Butler
 */
public interface ScreenService 
{
	/**
	 * Retrieve a list of {@link Screen} objects belonging to the {@link Cinema} object
	 * as specified by the <code>cinemaId</code> value.
	 * @param cinemaId The id of the {@link Cinema} object to retrieve associated {@link Screen} objects
	 * @return associated {@link Screen} objects
	 */
	public List<Screen> getScreensByCinemaId(long cinemaId);
	
	public void saveScreen(Screen screen);
}
