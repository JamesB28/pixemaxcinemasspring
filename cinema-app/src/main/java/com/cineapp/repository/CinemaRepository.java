package com.cineapp.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.cineapp.model.Cinema;

public interface CinemaRepository extends CrudRepository<Cinema, Long>
{
	@Query("SELECT c FROM Cinema c "
			+ "JOIN FETCH c.films f "
			+ "WHERE c.id = ?1 ")
	public Cinema findByIdFetchFilms(long cinemaId);
}
