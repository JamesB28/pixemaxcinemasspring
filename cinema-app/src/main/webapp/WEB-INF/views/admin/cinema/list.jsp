<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<h2>Cinema List</h2>
<p>
	<a class="btn btn-success" href="${pageContext.servletContext.contextPath}/admin/cinema/create"><span class="glyphicon glyphicon-plus-sign"></span> Add</a>
</p>
<table class="table table-hover">
	<tr>
		<th>Name</th>
		<th>City</th>
		<th></th>
	</tr>
	<c:forEach var="cinema" items="${cinemas}">
		<tr>
			<td><c:out value="${cinema.name}" /></td>
			<td><c:out value="${cinema.city}" /></td>
			<td><a class="btn btn-info" href="${pageContext.servletContext.contextPath}/admin/cinema/screens/edit/${cinema.id}"><span class="glyphicon glyphicon-cog"></span> Edit Screens</a></td>
		</tr>
	</c:forEach>
</table>