package com.cineapp.controller.administration;

import javax.validation.Valid;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.cineapp.model.Film;
import com.cineapp.model.validators.FilmValidator;
import com.cineapp.service.CinemaService;
import com.cineapp.service.FilmService;

@Controller
@RequestMapping("/admin/film")
public class FilmController 
{
	
	@Autowired
	private FilmService filmService;
	
	@Autowired
	private CinemaService cinemaService;
	
	@Autowired
	private FilmValidator filmValidator;
	
	public static final String LIST_URL = "/list";
	public static final String CREATE_URL = "/create";
	public static final String LIST_FULL_URL = "/admin/film/list";
	public static final String CREATE_FULL_URL = "/admin/film/create";
	
	/**
	 * Retrieve the list of films currently held by the system.
	 * @return A <code>ModelAndView</code> object containing a list of {@film} objects
	 */
	@RequestMapping(value=LIST_URL, method = RequestMethod.GET)
	public ModelAndView getFilmList()
	{
		ModelAndView modelAndView = new ModelAndView(LIST_FULL_URL);
		modelAndView.addObject("films", filmService.getAllFilms());
		return modelAndView;
	}
	
	/**
	 * Retrieve the models required to populate a new form for creating a 
	 * new {@link Film} in the system.
	 * @return A <code>ModelAndView</code> object populated with required 
	 * {@link Film} creation objects
	 */
	@RequestMapping(value=CREATE_URL, method = RequestMethod.GET)
	public ModelAndView getCreateFilmForm()
	{
		ModelAndView modelAndView = new ModelAndView(CREATE_FULL_URL);
		Film newFilm = new Film();		
		newFilm.setShowingFrom(new DateTime());
		newFilm.setShowingTo(new DateTime().plusDays(7));
		modelAndView.addObject("film", newFilm);
		modelAndView.addObject("cinemaList", cinemaService.findAll());
		return modelAndView;
	}
	
	/**
	 * Adds a new {@link Film} to the system, associating it to the {@link Cinema} objects
	 * as specified by the <code>cinemaIds</code> id values. Each {@link Film} must be 
	 * associated to at least one {@link Cinema}. 
	 * @param cinemaIds The {@link Cinema} objects to associate the new {@link Film} to.
	 * @param newFilm The new {@link Film} object to create.
	 * @param result A {@link BindingResult} containing any model bind errors.
	 * @return A {@link ModelAndView} object redirecting the user to the appropriate view.
	 */
	@RequestMapping(value=CREATE_URL, method = RequestMethod.POST)
	public ModelAndView createNewFilm( 	
			@ModelAttribute("film") @Valid Film newFilm, BindingResult result)
	{
		filmValidator.validate(newFilm, result);
		
		if (result.hasErrors())
		{
			return getCreateFilmFormWithErrors(newFilm);
		}
		
		if (!filmService.addNewFilm(newFilm))
		{
			ModelAndView modelAndView = getCreateFilmFormWithErrors(newFilm);
			modelAndView.addObject("errorMessage", "general_error.service_error_on_persist");
			return modelAndView;
		}
		
		ModelAndView modelAndView = getFilmList();
		modelAndView.addObject("filmName", newFilm.getName());
		modelAndView.addObject("successMessage", "general_label.successfully_added_to_system");
		return modelAndView;		
	}
	
	/**
	 * Create and re-populate the create {@link Film} form with the data populated 
	 * by the user currently.
	 * @param newFilm The new {@link Film} object to re-populate the forms details with
	 * @param cinemaIds The currently selected cinemaIds
	 * @return A {@link ModelAndView} populated with data to create a new {@link Film}
	 */
	private ModelAndView getCreateFilmFormWithErrors(Film newFilm)
	{
		ModelAndView modelAndView = new ModelAndView(CREATE_FULL_URL);
		modelAndView.addObject("film", newFilm);
		modelAndView.addObject("cinemaList", cinemaService.findAll());
		return modelAndView;
	}
}
