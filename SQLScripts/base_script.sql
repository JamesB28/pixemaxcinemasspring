CREATE SCHEMA `pixemaxcinemas`;

CREATE TABLE `pixemaxcinemas`.`Cinema` 
(`id` BIGINT NOT NULL AUTO_INCREMENT, 
`name` VARCHAR(45) NULL, 
`city` VARCHAR(100) NULL, 
PRIMARY KEY (`id`));

