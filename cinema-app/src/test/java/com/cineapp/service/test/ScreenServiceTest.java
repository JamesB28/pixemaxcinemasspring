package com.cineapp.service.test;

import java.util.ArrayList;
import java.util.List;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.testng.annotations.Test;

import static org.testng.Assert.*;
import static org.mockito.Mockito.*;

import com.cineapp.model.Screen;
import com.cineapp.repository.ScreenRepository;
import com.cineapp.service.ScreenService;
import com.cineapp.service.ScreenServiceImpl;
import com.cineapp.testcomponents.TestBase;

public class ScreenServiceTest extends TestBase
{

	@Mock
	private ScreenRepository screenRepositoryMock;
	
	@InjectMocks
	private ScreenService screenServiceImplMock = new ScreenServiceImpl();
	
	/**
	 * Ensure that an invalid {@link Cinema} <code>id</code> is passed to the service,
	 * the 
	 */
	@Test
	public void getScreensByCinemaIdTest_InvalidCinemaId()
	{
		//Arrange
		long cinemaId = 0l;
		
		//Act
		List<Screen> result = screenServiceImplMock.getScreensByCinemaId(cinemaId);
		
		//Assert
		verify(screenRepositoryMock, never()).findByCinemaId(anyLong());
		assertNull(result);
	}
	
	/**
	 * Ensure that when a valid {@link Cinema} <code>id</code> is specified, 
	 * the screen repository has a request made against it.
	 */
	@Test
	public void getScreensByCinemaIdTest_Success()
	{
		//Arrange
		long cinemaId = 4l;
		List<Screen> sampleScreens = new ArrayList<>();
		sampleScreens.add(new Screen());
		when(screenRepositoryMock.findByCinemaId(cinemaId)).thenReturn(sampleScreens);
		
		//Act
		List<Screen> result = screenServiceImplMock.getScreensByCinemaId(cinemaId);
		
		//Assert
		assertNotNull(result);
		assertEquals(1, result.size());
	}
	
	@Test
	public void saveScreenTest_Success()
	{
		//Arrange
		
		//Act
		
		//Assert
	}
}
