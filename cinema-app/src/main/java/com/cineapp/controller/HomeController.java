package com.cineapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.cineapp.model.uicomposite.CinemaFilmListing;
import com.cineapp.service.CinemaService;
import com.cineapp.service.FilmService;

/**
 * Handles server requests for the home page of the site
 * @author James Butler
 */
@Controller
public class HomeController 
{
	@Autowired
	private CinemaService cinemaService;
	
	@Autowired
	private FilmService filmService;
	
	private static final String HOME = "home";
	
	/**
	 * Default mapping for the root of the website
	 * @param model 
	 * @return A {@link ModelAndView} object containing the view and all data required
	 * for display on the view.
	 */
	@RequestMapping(value = {"/", "/home"}, method = RequestMethod.GET)
	public ModelAndView printWelcome(ModelMap model)
	{
		ModelAndView modelAndView = new ModelAndView(HOME);
		model.addAttribute("message", "Welcome to Pixemax Cinemas!");
		modelAndView.addObject("cinemaList", cinemaService.findAll());
		return modelAndView;
	}
	/**
	 * Controller to handle an AJAX request for the retrieval of films for a particular cinema
	 * @param cinemaId The id of the cinema to find current films for.
	 * @return A {@link CinemaFilmListing} composite object containing a list of films
	 */
	@RequestMapping(value = "/films/{cinemaId}", method = RequestMethod.POST)
	public @ResponseBody CinemaFilmListing getFilmsForCinema(@PathVariable(value="cinemaId") long cinemaId)
	{
		if (cinemaId > 0)
		{
			CinemaFilmListing cinemaFilmListing = 
					new CinemaFilmListing(filmService.getCurrentFilmsForCinema(cinemaId), cinemaId);
			
			return cinemaFilmListing;
		}
		else
			return null;
	}
}
