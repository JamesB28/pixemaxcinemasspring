<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!--
<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
  	
   
  	<!-- Indicators 
	<ol class="carousel-indicators">
    	<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
    	<li data-target="#carousel-example-generic" data-slide-to="1"></li>
    	<li data-target="#carousel-example-generic" data-slide-to="2"></li>
  	</ol>

  	<!-- Wrapper for slides
  	<div class="carousel-inner">
    	<div class="item active">
      		<img src="#" alt="#">
	      	<div class="carousel-caption">
	        
	      	</div>
    	</div>
	</div>

  	<!-- Controls
  	<a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
    	<span class="glyphicon glyphicon-chevron-left"></span>
  	</a>
  	<a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
    	<span class="glyphicon glyphicon-chevron-right"></span>
  	</a>
</div>
 -->
<div class="row">
	<div class="col-sm-4 center">
		<select id="cinemaList" class="form-control">
			<option value="0">Select a Cinema</option>
			<c:forEach var="cinema" items="${cinemaList}" >
				<option value='<c:out value="${cinema.id}" />'> ${cinema.name}</option>
			</c:forEach>
		</select>
	</div>
</div>
<br />
<div id="insertHere">
</div>