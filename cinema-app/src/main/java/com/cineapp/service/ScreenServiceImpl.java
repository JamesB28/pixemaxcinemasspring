package com.cineapp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cineapp.model.Screen;
import com.cineapp.repository.ScreenRepository;

@Service
public class ScreenServiceImpl implements ScreenService
{
	@Autowired
	private ScreenRepository screenRepository;
	
	/*
	 * (non-Javadoc)
	 * @see com.cineapp.service.ScreenService#getScreensByCinemaId(long)
	 */
	public List<Screen> getScreensByCinemaId(long cinemaId) 
	{
		if (cinemaId > 0)
		{
			return screenRepository.findByCinemaId(cinemaId);
		}
		return null;
	}


	public void saveScreen(Screen screen)
	{
		screenRepository.save(screen);
	}
}
