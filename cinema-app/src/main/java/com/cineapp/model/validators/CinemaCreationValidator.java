package com.cineapp.model.validators;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.cineapp.model.uicomposite.CinemaCreation;

@Component
public class CinemaCreationValidator implements Validator
{

	@Override
	public boolean supports(Class<?> clazz) 
	{
		return CinemaCreation.class.equals(clazz);
	}

	@Override
	public void validate(Object obj, Errors errors) 
	{
		CinemaCreation cinemaCreation = (CinemaCreation) obj;
		
		if (cinemaCreation.getCinema() == null)
		{
			errors.rejectValue("cinemaCreation.cinema", "Cinema cannot be null");
		}
		else
		{
			if (cinemaCreation.getCinema().getName() == null || cinemaCreation.getCinema().getName().equals(""))
			{
				errors.rejectValue("cinema.name", "cinema.name.empty", "The Cinema name cannot be left empty.");
			}
			if (cinemaCreation.getCinema().getCity() == null || cinemaCreation.getCinema().getCity().equals(""))
			{
				errors.rejectValue("cinema.city", "cinema.city.empty", "The Cinema city cannot be left empty.");
			}
		}
	}
}
