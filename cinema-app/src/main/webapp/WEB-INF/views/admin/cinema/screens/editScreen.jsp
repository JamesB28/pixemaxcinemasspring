<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<h2>Edit Screens</h2>
<table class="table table-hover">
	<tr>
		<th>Name</th>
		<th>Number of Rows</th>
		<th>Seats per Row</th>
		<th></th>
	</tr>
	<c:forEach var="screen" items="${screensForCinema}">
		<tr>
			<td>${screen.name}</td>
			<td>${screen.numRows}</td>
			<td>${screen.numSeatsPerRow}</td>
			<td><a class="btn btn-info" href="#" data-toggle="modal" data-target="#myModal${screen.id}"><span class="glyphicon glyphicon-cog"></span> Edit Screen</a></td>
		</tr>
	</c:forEach>	
</table>
<svg id="snapsvg" width="500" height="500"></svg>
<br />
<a class="btn btn-default" href="${pageContext.servletContext.contextPath}/admin/cinema/list"><span class="glyphicon glyphicon-chevron-left"></span> Back</a>

<c:forEach var="screen" items="${screensForCinema}">
	<form:form method="POST" modelAttribute="screen" class="form-horizontal" role="form">
		<div class="modal fade" id="myModal${screen.id}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
			    <div class="modal-content">
		      		<div class="modal-header">
			        	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			        	<h4 class="modal-title" id="myModalLabel">Editing Screen: ${screen.name}</h4>
			      	</div>
			      	<input type="hidden" name="id" value="${screen.id}"/>
			      	<div class="modal-body">
			        	<div class="form-group">
							<label for="name" class="col-sm-5 control-label">Name</label>
							<div class="col-sm-5">
								<input type="text" name="name" value="${screen.name}" class="form-control"/>
							</div>
						</div>
						<div class="form-group">
							<label for="name" class="col-sm-5 control-label">Number of rows</label>																
							<div class="col-sm-5">
								<input id="numRowsScreen${screen.id}" type="text" name="numRows" value="${screen.numRows}"class="form-control"/>
							</div>
						</div>
						<div class="form-group">
							<label for="name" class="col-sm-5 control-label">Number of seats per row</label>														
							<div class="col-sm-5">
								<input id="numSeatsScreen${screen.id}" type="text" name="numSeatsPerRow" value="${screen.numSeatsPerRow}" class="form-control"/>
							</div>
						</div>
						<button id="designScreenBtn${screen.id}" type="button" class="btn btn-default pull-right">Design Screen</button>
						<svg id="screenDesignId${screen.id}" width="500" height="500"></svg>
			      	</div>			      	
			      	<div class="modal-footer">
			        	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			        	<button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-ok-circle"></span> Update</button>
			      	</div>
			    </div>
		  	</div>
		</div>
	</form:form>
</c:forEach>