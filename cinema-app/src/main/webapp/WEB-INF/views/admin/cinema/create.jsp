<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<h2>Add a new Cinema</h2>
<form:form method="POST" modelAttribute="cinemaCreationObj" class="form-horizontal" role="form">
	<div class="form-group">
		<label for="name" class="col-sm-2 control-label">Name</label>
		<div class="col-sm-5">
			<form:input path="cinema.name" class="form-control"/>
		</div>
		<p class="help-block"><form:errors path="cinema.name" /></p>
	</div>
	
	<div class="form-group">
		<label for="name" class="col-sm-2 control-label">City</label>
		<div class="col-sm-5">
			<form:input path="cinema.city" type="text" class="form-control"/>
		</div>
		<p class="help-block"><form:errors path="cinema.city" /></p>
	</div>
	
	<div class="form-group">
		<label for="numScreens" class="col-sm-2 control-label">Number of Screens</label>
		<div class="col-sm-1">
			<form:select path="numScreens"  class="form-control">
				<form:options items="${cinemaCreationObj.screenDropdownValues}" />
			</form:select>
		</div>
		<p class="help-block">Default screens will be added, these can be customised at a later stage.</p>
	</div>
	
	<div class="form-group">
    	<div class="col-sm-offset-2 col-sm-10">
			<button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-ok-circle"></span> Create</button>
		</div>
	</div>
</form:form>