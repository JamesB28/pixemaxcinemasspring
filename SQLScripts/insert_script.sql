use pixemaxcinemas;

/* Insert into Cinema table */
INSERT INTO cinema (city, name)
VALUES 
	('Arcadia', 'Pixemax Arcadia'),
	('Bergania', 'Pixemax Bergania');

/* Insert into Screen table */
INSERT INTO screen (name, numberOfRows, numberOfSeatsPerRow, cinemaId)
VALUES ('Screen 1', 5, 20, 1);

/* Insert Films */
INSERT INTO film (description, name, runTime, showingFrom, showingTo)
VALUES 
	('When Batman, Gordon and Harvey Dent launch an assault on the mob, they let the clown out of the box, the Joker, bent on turning Gotham on itself and bringing any heroes down to his level.', 'The Dark Knight', 152, NOW(), DATE_ADD(NOW(), INTERVAL 7 DAY)),
	('A process server and his marijuana dealer wind up on the run from hitmen and a corrupt police officer after he witness his dealer\'s boss murder a competitor while trying to serve papers on him.', 'Pineapple Express', 111, NOW(), DATE_ADD(NOW(), INTERVAL 7 DAY));

INSERT INTO cinemafilms(cinemaId, filmId)
VALUES
(1, 1),
(1, 2);

INSERT INTO showing(showDateTime, filmId, screenId)
VALUES(DATE_ADD(NOW(), INTERVAL 7 DAY), 1, 1);