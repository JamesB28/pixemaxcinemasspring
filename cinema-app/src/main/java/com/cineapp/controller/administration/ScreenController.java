package com.cineapp.controller.administration;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.cineapp.model.Screen;
import com.cineapp.service.ScreenService;

/**
 * Controller to manage the updating of {@link Screen} information 
 * @author James Butler
 */
@Controller
@RequestMapping("/admin/cinema/screens")
public class ScreenController 
{
	@Autowired
	private ScreenService screenService;
	
	private static final String CINEMA_ID = "/edit/{cinemaId}";
	private static final String EDIT_SCREEN = "/admin/cinema/screens/editScreen";
	private static final String UPDATE_SCREEN = "/edit/{cinema}";
	
	@RequestMapping(value = CINEMA_ID, method = RequestMethod.GET)
	public ModelAndView getScreensForCinema(@PathVariable(value="cinemaId") long cinemaId)
	{
		ModelAndView modelAndView = new ModelAndView();
		
		if (cinemaId > 0)
		{
			List<Screen> screensForCinema = screenService.getScreensByCinemaId(cinemaId);
			modelAndView.setViewName(EDIT_SCREEN);
			modelAndView.addObject("screensForCinema", screensForCinema);
			modelAndView.addObject("screen", new Screen());
		}
		else
		{
			//Cinema could not be found
		}
		
		return modelAndView;
	}
	
	@RequestMapping(value = UPDATE_SCREEN, method = RequestMethod.POST)
	public ModelAndView updateScreenValue(
			@ModelAttribute("screen") Screen updatedScreen, 
			BindingResult validationResult)
	{
		screenService.saveScreen(updatedScreen);
		
		//need to get the cinema belonging to the screen being updated
		
		return getScreensForCinema(updatedScreen.getCinema().getId());
	}
}
