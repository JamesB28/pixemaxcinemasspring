$( document ).ready(function() {
	
	/* Listener for change of Cinema */ 
	$('#cinemaList').on('change', function(){
		console.log('I will change some ajax now.');
		
		console.log('selected cinema id is: ' + $("#cinemaList").val());
		
		$('.tmp').remove();    // Remove any divs added by the last request
			 
		if (request) {
		  request.abort();  // abort any pending request
		} 
		
		/* The hard coded URL needs to either be dynamically retrieved or moved to a global config */
		var request = $.ajax({
			url : "http://localhost:8080/pixemax-cinemas/films/" + $("#cinemaList").val(),
			type : "POST", 
			data : $("cinemaList").val()
		});
		
		request.done(function(data)
		{
			if (data.films != null){
				console.log("I got some data!");
				$("Films for cinema are")
				.appendTo("#insertHere");
				var films = data.films;
				
				for (var i = 0; i < films.length; i++){
					var film = films[i];
					var newPara = 
						'<div class="row tmp">' +
							'<div class="col-xs-2">' + 'image here' + '</div>' +
							'<div class="col-xs-8">' 
								+ '<h3>'+ film.name + '</h3>'
								+ '<p><small>Runtime approx. '+ film.runTime +' minutes </small> <br />' 
								+ film.description +
							'</p></div>' +
						'</div>';
					$(newPara).appendTo("#insertHere");
				}
			}
		});

	});
});