package com.cineapp.model.uicomposite;

import javax.validation.Valid;

import org.springframework.stereotype.Component;

import com.cineapp.model.Cinema;

/**
 * Created for the population of properties required to create a new
 * Cinema object and associated number of default screens.
 */
@Component
public class CinemaCreation 
{
	private Cinema cinema;
	private int numScreens;
	private int[] screenDropdownValues;
	
	public CinemaCreation(){}
	
	public CinemaCreation(@Valid Cinema cinema, int[] screenDropdownValues)
	{
		this.cinema = cinema;
		this.screenDropdownValues = screenDropdownValues;
	}
	
	public Cinema getCinema() 
	{
		return cinema;
	}
	public void setCinema(Cinema cinema) 
	{
		this.cinema = cinema;
	}
	public int getNumScreens() 
	{
		return numScreens;
	}
	public void setNumScreens(int numScreens) 
	{
		this.numScreens = numScreens;
	}
	public int[] getScreenDropdownValues() 
	{
		return screenDropdownValues;
	}
	public void setScreenDropdownValues(int[] screenDropdownValues) 
	{
		this.screenDropdownValues = screenDropdownValues;
	}
}
