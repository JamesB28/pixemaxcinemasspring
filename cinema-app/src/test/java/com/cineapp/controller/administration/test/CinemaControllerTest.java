package com.cineapp.controller.administration.test;

import java.util.ArrayList;
import java.util.List;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.web.servlet.ModelAndView;
import org.testng.annotations.Test;

import static org.testng.Assert.*;
import static org.mockito.Mockito.*;

import com.cineapp.controller.administration.CinemaController;
import com.cineapp.model.Cinema;
import com.cineapp.model.uicomposite.CinemaCreation;
import com.cineapp.service.CinemaService;
import com.cineapp.testcomponents.TestBase;

public class CinemaControllerTest extends TestBase 
{
	@Mock
	private CinemaService cinemaServiceMock;
	
	@InjectMocks
	private CinemaController cinemaControllerMock;
	
	//Expected redirect URLs
	private static final String LIST_FULL_URL = "/admin/cinema/list";
	private static final String CREATE_FULL_URL = "/admin/cinema/create";
	
	/**
	 * Ensure that a list of {@link Cinema} objects is correctly requested from the 
	 * service and returned by the controller 
	 */
	@Test
	@SuppressWarnings("unchecked")
	public void getCinemaListTest()
	{
		//Arrange
		List<Cinema> cinemaList = new ArrayList<Cinema>();
		cinemaList.add(new Cinema());
		when(cinemaServiceMock.findAll()).thenReturn(cinemaList);
		
		//Act
		ModelAndView modelAndView = cinemaControllerMock.getCinemaList();
		
		//Assert
		assertEquals(LIST_FULL_URL, modelAndView.getViewName());	
		ArrayList<Cinema> retrievedCinemas =(ArrayList<Cinema>) modelAndView.getModelMap().get("cinemas");
		assertNotNull(retrievedCinemas);
		assertEquals(1, retrievedCinemas.size());
	}
	
	/**
	 * Ensure that a cinema creation object is correctly returned for population
	 */
	@Test
	public void getCreateCinemaFormTest()
	{
		//Arrange
		//Act
		ModelAndView modelAndView = cinemaControllerMock.getCreateCinemaForm();
		
		//Assert
		assertEquals(CREATE_FULL_URL, modelAndView.getViewName());		
		CinemaCreation returnedCreationObj = (CinemaCreation)modelAndView.getModelMap().get("cinemaCreationObj");
		assertNotNull(returnedCreationObj);
		assertTrue(returnedCreationObj.getScreenDropdownValues().length > 0);
	}
}
