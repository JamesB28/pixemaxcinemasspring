package com.cineapp.controller.administration.test;

import java.util.ArrayList;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.ModelAndView;
import org.testng.annotations.Test;

import com.cineapp.controller.administration.FilmController;
import com.cineapp.model.Cinema;
import com.cineapp.model.Film;
import com.cineapp.model.validators.FilmValidator;
import com.cineapp.service.CinemaService;
import com.cineapp.service.FilmService;
import com.cineapp.testcomponents.TestBase;

import static org.testng.Assert.*;
import static org.mockito.Mockito.*;

@SuppressWarnings("unchecked")
public class FilmControllerTest extends TestBase 
{

	@Mock
	private FilmService filmServiceMock;
	
	@Mock
	private CinemaService cinemaServiceMock;
	
	@Mock
	private FilmValidator filmValidatorMock;
	
	@InjectMocks
	private FilmController filmController;
	
	public static final String LIST_URL = "/list";
	public static final String CREATE_URL = "/create";
	public static final String LIST_FULL_URL = "/admin/film/list";
	public static final String CREATE_FULL_URL = "/admin/film/create";
	
	/**
	 * Ensure that the list of films is correctly retrieved from the service
	 */
	@Test
	public void getFilmListTest()
	{
		//Arrange
		when(filmServiceMock.getAllFilms()).thenReturn(getSampleFilmList());
		
		//Act
		ModelAndView modelAndView = filmController.getFilmList(); 
		
		//Assert
		assertEquals(LIST_FULL_URL, modelAndView.getViewName());
		ArrayList<Film> retrievedFilms = (ArrayList<Film>)  modelAndView.getModelMap().get("films");
		assertNotNull(retrievedFilms);
		assertEquals(2, retrievedFilms.size());
	}
	
	/**
	 * Ensure the {@link Film} create form returns all the required entities 
	 * for successful creation.
	 */
	@Test
	public void getCreateFilmFormTest()
	{
		//Arrange
		when(cinemaServiceMock.findAll()).thenReturn(getSampleCinemaList());
		
		//Act
		ModelAndView modelAndView = filmController.getCreateFilmForm();		
		
		//Assert
		assertEquals(CREATE_FULL_URL, modelAndView.getViewName());
		ArrayList<Cinema> retrievedCinemas = (ArrayList<Cinema>) modelAndView.getModelMap().get("cinemaList");
		assertNotNull(retrievedCinemas);
		assertEquals(2, retrievedCinemas.size());
		Film film = (Film) modelAndView.getModelMap().get("film");
		assertNotNull(film);
		assertNotNull(film.getShowingFrom());
		assertNotNull(film.getShowingTo());		
	}
	
	/**
	 * Ensure that when a {@link BindingResult} with errors is returned from
	 * the form, the redirect is handled appropriately.
	 */
	@Test
	public void createNewFilmTest_ModelErrors()
	{
		//Arrange
		Film newFilm = new Film();
		BindingResult result = mock(BindingResult.class);
		when(result.hasErrors()).thenReturn(true);
		when(cinemaServiceMock.findAll()).thenReturn(getSampleCinemaList());
		
		//Act
		ModelAndView modelAndView = filmController.createNewFilm(newFilm, result);
				
		//Assert
		assertEquals(CREATE_FULL_URL, modelAndView.getViewName());
		ArrayList<Cinema> retrievedCinemas = (ArrayList<Cinema>) modelAndView.getModelMap().get("cinemaList");
		assertNotNull(retrievedCinemas);
		assertEquals(2, retrievedCinemas.size());
		Film film = (Film) modelAndView.getModelMap().get("film");
		assertNotNull(film);
	}
	
	/**
	 * Ensure that when the new {@link Film} has been passed to the service
	 * and <code>false</code> is returned, the 
	 */
	@Test
	public void createNewFilmTest_ServiceReturnsFalse()
	{
		//Arrange
		Film newFilm = new Film();
		BindingResult result = mock(BindingResult.class);
		when(result.hasErrors()).thenReturn(false);
		when(filmServiceMock.addNewFilm(any(Film.class))).thenReturn(false);
		when(cinemaServiceMock.findAll()).thenReturn(getSampleCinemaList());
		
		//Act
		ModelAndView modelAndView = filmController.createNewFilm(newFilm, result);
		
		//Assert
		assertEquals(CREATE_FULL_URL, modelAndView.getViewName());
		ArrayList<Cinema> retrievedCinemas = (ArrayList<Cinema>) modelAndView.getModelMap().get("cinemaList");
		assertNotNull(retrievedCinemas);
		assertEquals(2, retrievedCinemas.size());
		Film film = (Film) modelAndView.getModelMap().get("film");
		assertNotNull(film);
		assertNotNull(modelAndView.getModelMap().get("errorMessage"));
	}
	
	/**
	 * Ensure when a new {@link Film} object is successfully submitted to 
	 * the system, the correct view and message is displayed to the user.
	 */
	@Test
	public void createNewFilmTest_Success()
	{
		//Arrange
		Film newFilm = new Film();
		String newFilmName = "newFilm";
		newFilm.setName(newFilmName);
		BindingResult result = mock(BindingResult.class);
		when(result.hasErrors()).thenReturn(false);
		when(filmServiceMock.addNewFilm(any(Film.class))).thenReturn(true);
		when(filmServiceMock.getAllFilms()).thenReturn(getSampleFilmList());
		
		//Act
		ModelAndView modelAndView = filmController.createNewFilm(newFilm, result);
		
		//Assert
		assertEquals(LIST_FULL_URL, modelAndView.getViewName());
		ArrayList<Film> retrievedFilms = (ArrayList<Film>)  modelAndView.getModelMap().get("films");
		assertNotNull(retrievedFilms);
		assertEquals(2, retrievedFilms.size());
		assertTrue(newFilmName.equals(modelAndView.getModelMap().get("filmName")));
		assertNotNull(modelAndView.getModelMap().get("successMessage"));
	}
	
	/**
	 * @return A sample <code>ArrayList</code> containing two {@link Cinema} objects.
	 */
	private ArrayList<Cinema> getSampleCinemaList()
	{
		Cinema cinema1 = new Cinema();
		Cinema cinema2 = new Cinema();
		ArrayList<Cinema> cinemaList = new ArrayList<>();
		cinemaList.add(cinema1);
		cinemaList.add(cinema2);
		return cinemaList;
	}
	
	/**
	 * @return A sample <code>ArrayList</code> containing two {@link Film} objects.
	 */
	public ArrayList<Film> getSampleFilmList()
	{
		Film film1 = new Film();
		Film film2 = new Film();
		ArrayList<Film> filmList = new ArrayList<>();
		filmList.add(film1);
		filmList.add(film2);	
		return filmList;
	}
}