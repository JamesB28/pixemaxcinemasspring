package com.cineapp.service.test;

import java.util.ArrayList;
import java.util.List;

import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.testng.annotations.Test;

import static org.testng.Assert.*;
import static org.mockito.Mockito.*;

import com.cineapp.model.Cinema;
import com.cineapp.repository.CinemaRepository;
import com.cineapp.service.CinemaService;
import com.cineapp.service.CinemaServiceImpl;
import com.cineapp.testcomponents.TestBase;

public class CinemaServiceTest extends TestBase 
{

	@Mock
	private CinemaRepository cinemaRepositoryMock;
	
	@InjectMocks
	private CinemaService cinemaServiceImplMock = new CinemaServiceImpl();
	
	/*
	 * Verify that the find all method correctly requests the data from the service
	 */
	@Test
	public void findAllTest() 
	{
		//Arrange
		List<Cinema> sampleList = new ArrayList<Cinema>();
		Cinema cinema = new Cinema();
		sampleList.add(cinema);
		when(cinemaRepositoryMock.findAll()).thenReturn(sampleList);
		
		//Act
		List<Cinema> result = cinemaServiceImplMock.findAll();
		
		//Assert
		verify(cinemaRepositoryMock).findAll();
		assertEquals(sampleList, result);
	}
	
	/*
	 * Verify that the correct actions are undertaken upon calling the save
	 * action with valid input.
	 */
	@Test
	public void saveTest_ValidInput()
	{
		//Arrange
		Cinema cinema = new Cinema();
		ArgumentCaptor<Cinema> cinemaArgCap = ArgumentCaptor.forClass(Cinema.class);
				
		//Act
		cinemaServiceImplMock.save(cinema);
		
		//Assert
		verify(cinemaRepositoryMock).save(cinemaArgCap.capture());
		assertEquals(cinema, cinemaArgCap.getValue());
	}
	
	/*
	 * Verify that the correct actions are undertaken upon calling the save
	 * action with a null input.
	 */
	@Test
	public void saveTest_NullInput()
	{
		//Arrange								
		//Act
		cinemaServiceImplMock.save(null);
		
		//Assert
		verify(cinemaRepositoryMock, never()).save(any(Cinema.class));		
	}
}
