package com.cineapp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cineapp.model.Cinema;
import com.cineapp.repository.CinemaRepository;

@Service
public class CinemaServiceImpl implements CinemaService
{
	@Autowired
	private CinemaRepository cinemaRepository;
	/*
	 * (non-Javadoc)
	 * @see com.cineapp.service.CinemaService#findAll()
	 */
	public List<Cinema> findAll() 
	{	
		return (List<Cinema>) cinemaRepository.findAll();
	}

	/*
	 * (non-Javadoc)
	 * @see com.cineapp.service.CinemaService#save(com.cineapp.model.Cinema)
	 */
	public void save(Cinema cinema)
	{
		if (cinema != null)
		{
			cinemaRepository.save(cinema);
		}
	}
}
