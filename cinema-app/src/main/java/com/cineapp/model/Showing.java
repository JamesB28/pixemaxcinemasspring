package com.cineapp.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;

@Entity
@Table(name = "Showing")
public class Showing implements Serializable
{
	private static final long serialVersionUID = 2223786286419347244L;

	private long id;
	private DateTime showDateTime;
	private Film film;
	private Screen screen;
	
	public Showing(){}
	
	@Id 
	@GeneratedValue
	@Column(name = "id")
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	@Column(name = "showDateTime", nullable = false)
	public DateTime getShowDateTime() {
		return showDateTime;
	}
	public void setShowDateTime(DateTime showDateTime) {
		this.showDateTime = showDateTime;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "filmId")
	@ForeignKey(name = "FK_Showing_Film")
	public Film getFilm() {
		return film;
	}
	public void setFilm(Film film) {
		this.film = film;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "screenId")
	@ForeignKey(name = "FK_Showing_Screen")
	public Screen getScreen() {
		return screen;
	}
	public void setScreen(Screen screen) {
		this.screen = screen;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
	
}
